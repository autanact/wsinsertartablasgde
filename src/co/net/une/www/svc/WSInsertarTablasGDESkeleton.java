
/**
 * WSInsertarTablasGDESkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WSInsertarTablasGDESkeleton java skeleton for the axisService
     */
    public class WSInsertarTablasGDESkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WSInsertarTablasGDELocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param nombreTabla
                                     * @param nombreDataset
                                     * @param nombresCampos
                                     * @param valoresCampos
         */
        

                 public co.net.une.www.gis.WSInsertarTablasGDERSType insertarTablasGDE
                  (
                  java.lang.String nombreTabla,java.lang.String nombreDataset,java.lang.String nombresCampos,java.lang.String valoresCampos
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("nombreTabla",nombreTabla);params.put("nombreDataset",nombreDataset);params.put("nombresCampos",nombresCampos);params.put("valoresCampos",valoresCampos);
		try{
		
			return (co.net.une.www.gis.WSInsertarTablasGDERSType)
			this.makeStructuredRequest(serviceName, "insertarTablasGDE", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    